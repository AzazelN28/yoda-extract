#include <png.h>
#include <endian.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * Constantes.
 */
#define DTA_TILE_SIZE   1024        // 32x32
#define DTA_SETUP_SIZE  82944       // 288x288
#define DTA_HEADER_SIZE 4           // header

// ¿TIPOS DE ZONA?
#define DTA_ZONE_JUNGLE 1
#define DTA_ZONE_SNOW   2
#define DTA_ZONE_DESERT 3

/**
 * Establecemos los colores del tile.
 */
void setRGB(png_byte* ptr, uint8_t* color) {

    ptr[0] = color[2];
    ptr[1] = color[1];
    ptr[2] = color[0];

}

/**
 * tipo de estructura tile.
 */
typedef struct {

    uint32_t flags;
    uint8_t image[1024];

} dta_tile_t;

/**
 * setup
 */
typedef struct {

    uint8_t header[DTA_HEADER_SIZE];
    uint32_t length;
    uint8_t image[DTA_SETUP_SIZE];

} dta_setup_t;

/**
 * versión del documento.
 */
typedef struct {
    
    uint8_t reserved;
    uint8_t major;
    uint8_t minor;
    uint8_t revision;

} dta_version_t;

/**
 * Armamento de los personajes. 
 */
typedef struct {

    uint16_t character_id;
    
    uint16_t weapon_id;

    uint16_t unknown;

} dta_character_weapon_t;

typedef struct {

    uint32_t header;
    uint32_t unknown1;
    uint32_t unknown2;

} dta_izx4_t;

/**
 * Personajes.
 */
typedef struct {

    uint16_t id;
    
    uint8_t header[4];
    
    uint32_t length;
    
    uint8_t name[16];
    
    uint32_t unknown1;
    uint16_t unknown2;
    uint32_t unknown3;

    uint16_t images[24];

} dta_character_t;

/**
 * Nombres
 */
typedef struct {

    uint16_t id;
    uint8_t name[24];

} dta_name_t;

/**
 * 
 */
typedef struct {
    
    uint8_t header[4];
    uint32_t length;

} dta_action_t;

/**
 * DOCUMENTO completo.
 */
typedef struct {
    
    // VERSION
    dta_version_t* version;

    // SETUP
    dta_setup_t* setup;

    // TILE
    /*dta_tiles_t* tiles;

    // ZONES
    dta_zones_t* zones;

    // PUZZLES
    dta_puzzles_t* puzzles;

    // CHARACTERS
    dta_characters_t* characters;

    // WEAPONS.
    dta_character_weapons_t* weapons;

    // NAMES
    dta_names_t* names;*/

} dta_file;

/**
 * Esta es la función más importante en la lectura
 * de archivos DTA. Ya que es el encargado de leer 
 * toda la estructura de archivos en un único fichero.
 */
dta_file* dta_read(const char* filename) {

    //
    FILE* handler = NULL;

    // si no se puede abrir el documento devolvemos NULL;
    if (access(filename, F_OK) != 0) {
        return NULL;
    }

    // abrimos el documento para lectura.
    handler = fopen(filename, "r");
    if (handler == NULL) {
        return NULL;
    }
    
    // creamos el objeto dta.
    dta_file* dta = (dta_file*)malloc(sizeof(dta_file));

 
    return dta;

}

/**
 *
 */
void dta_free(dta_file* dta) {

    // TODO: esto deberá liberar la memoria
    //  de todo el archivo, esto implica liberar
    //  la que hemos reservado de forma dinámica.

    // liberamos la memoria reservada por el dta.
    free((void*)dta);

}

/**
 * dta_Block_header_check();
 *
 *  Este método reemplaza a strcmp.
 */
int dta_id_check(const uint8_t* expected, uint8_t* header) {
    return expected[0] == header[0] &&
           expected[1] == header[1] &&
           expected[2] == header[2] &&
           expected[3] == header[3];
}

/**
 * dta_block_header()
 *
 * Este método lee la cabecera del bloque.
 */
int dta_block_header(const char* expected, FILE* handler) {

    uint8_t header[4];
    fread(&header, 4, 1, handler);

    printf("Header: \033[1;31m%s\033[0m\n", header);

    if (!dta_id_check(expected, header)) {
        
        printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", header, expected);
        return EXIT_FAILURE;

    }

    return 1;

}

/**
 * dta_block_length()
 *
 * Este método lee la longitud del bloque.
 */
uint32_t dta_block_length(FILE* handler) {

    uint32_t length = 0;
    fread(&length, sizeof(uint32_t), 1, handler);
    printf("Length: \033[1;31m%d\033[0m\n", length);
    return length;

}

/**
 * dta_block_read_ZONE()
 *
 *  Leemos las zonas.
 */
uint32_t dta_block_read_ZONE(FILE* handler) {

    if (!dta_block_header("ZONE", handler)) {
        return EXIT_FAILURE;
    }

    uint16_t zones;
    fread(&zones, 2, 1, handler);

    printf("Zones: \033[1;31m%d\033[0m\n", zones);

    uint16_t index;

    uint16_t zoneType;
    uint32_t zoneLength;
    uint16_t zoneId;

    uint16_t unknown1;
    uint16_t unknown2;
    uint16_t unknown3;
    uint16_t unknown4;
    uint16_t unknown5;
    uint16_t unknown6;
    uint16_t unknown7;

    uint8_t izon[4];
    uint32_t izonLength;
    uint16_t izonWidth;
    uint16_t izonHeight;
    uint32_t izonFlags; // ESTAS BANDERAS INDICARÁN POR EJEMPLO, SI UN SECTOR ES CONECTABLE POR 1 LADO, O 2, O 3 O 4.

    uint8_t izax[4];
    uint32_t izaxLength;
    
    uint8_t izx2[4];
    uint32_t izx2Length;
    
    uint8_t izx3[4];
    uint32_t izx3Length;
    
    uint8_t izx4[4];
    uint32_t izx4Length;
    
    uint8_t iact[4];
    uint32_t iactLength;

    uint8_t path[1024];
        
    uint8_t x;
    uint8_t y;

    uint16_t bg;
    uint16_t md;
    uint16_t fg;

    // en las zonas siempre van
    // IZON - Esto es claramente de ZONE
    // IZAX - Zone Active X?
    // IZX2 - Zone Active X2?
    // IZX3
    // IZX4
    // IACT - Esto es claramente de ACTIONS
    //

    printf("\tZone: <IN>\t<TYPE> <LEN> <IDN> <LENG> W  H \n");
    for (index = 0; index < zones; index++) {

        // ESTO VA A SER EL TIPO DE ZONA CASI SEGURO.
        fread(&zoneType, 2, 1, handler);
        fread(&zoneLength, 4, 1, handler);
        fread(&zoneId, 2, 1, handler);


        fread(&izon, 4, 1, handler);
        if (!dta_id_check("IZON", izon)) {
            printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", izon, "IZON");
            return EXIT_FAILURE;
        }

        // tamaño de la zona en bytes (incluyendo cabecera).
        fread(&izonLength, 4, 1, handler);

        // tamaño de la zona.
        fread(&izonWidth, 2, 1, handler);
        fread(&izonHeight, 2, 1, handler);

        // banderas usadas para limitar 
        // la zona.
        fread(&izonFlags, 4, 1, handler);

        // ¿?¿? ESTO SIEMPRE ES 0xFFFF ¿?¿?¿?
        fread(&unknown4, 2, 1, handler);

        // esto siempre va desde 1, 2, 3 o 5. ¿MÁS FLAGS?
        fread(&unknown5, 2, 1, handler);
        
        printf("\tZone: \033[1;31m%04d\t%06d %05d %05d %06d %02d %02d %05d %05d %05d\033[0m\n", 
                index, 
                zoneType, 
                zoneLength, 
                zoneId, 
                izonLength, 
                izonWidth, 
                izonHeight,
                izonFlags,
                unknown4,
                unknown5
            );

        sprintf(path, "ZONES/%04d.json", zoneId);

        FILE* zoneHandler = fopen(path, "w");
        if (zoneHandler == NULL) {
            printf("\033[1;31mCannot create file %s\033[0m\n", zoneHandler);
            return EXIT_FAILURE;
        }

        fprintf(zoneHandler, "{\n\t\"id\": %d,\n\t\"type\": %d,\n\t\"width\": %d,\n\t\"height\": %d,\n\t\"flags\": %d,\n", zoneId, zoneType, izonWidth, izonHeight, izonFlags);

        fprintf(zoneHandler, "\t\"tiles\": [\n");
        // dibujamos el mapa.
        for (y = 0; y < izonHeight; y++) {
            
            fprintf(zoneHandler, "\t\t[\n");
            for (x = 0; x < izonWidth; x++) {
                
                // leemos fondo, medio y frente.
                fread(&bg, 2, 1, handler);
                fread(&md, 2, 1, handler);
                fread(&fg, 2, 1, handler);

                if (bg != 0xFFFF) {
                    putchar('B');
                } else {
                    putchar(32);
                }
                
                if (md != 0xFFFF) {
                    putchar('M');
                } else {
                    putchar(32);
                }
                
                if (fg != 0xFFFF) {
                    putchar('F');
                } else {
                    putchar(32);
                }

                fprintf(zoneHandler, "\t\t\t[%d,%d,%d]", bg, md, fg);

                if (x == izonWidth - 1) {
                    fprintf(zoneHandler,"\n");
                } else {
                    fprintf(zoneHandler,",\n");
                }

            }

            fprintf(zoneHandler, "\t\t]");
            if (y == izonHeight - 1) {
                fprintf(zoneHandler,"\n");
            } else {
                fprintf(zoneHandler,",\n");
            }

            printf("\n");

        }

        fprintf(zoneHandler, "\t]\n");

        fprintf(zoneHandler, "}\n");

        fclose(zoneHandler);

        zoneLength -= 24;
        zoneLength -= (izonWidth * izonHeight * 6);

        fread(&unknown6, 2, 1, handler);

        // Esto contiene elementos ESPECIALES
        // dentro de la zona, esto quiere decir
        // que a partir de aquí lo que se leen
        // son:
        //
        //  - COMIENZO? (0000)?
        //  - ¿? (0001)?
        //  - ¿? (0002)?
        //  - TILE? (0003)? SALIDAS?
        //  - TILE? (0004)? CONEXIONES?
        //  - ¿? (0005)?
        //  - ITEMS (0006)
        //  - CHARACTERS (0007)
        //  - WEAPONS (0008)
        //  - SALIDAS/CONEXIONES (0009)?
        //  - RETORNO (0010)?
        //  - NO APARECE? (0011)?
        //  - ¿? (0012)?
        //  - ¿? (0013)?
        //  - ¿? (0014)?
        //  - ¿? (0015)?
        //
        // Los campos son:
        //
        //  - tipo 4 bytes
        //  - x 2 bytes
        //  - y 2 bytes
        //  - siempre parece ser 1 (2 bytes)
        //  - id (en función del tipo puede indicar una cosa u otra).
        printf("\nZoneChars?\n");

        uint16_t i;
        uint16_t j;
        uint32_t unknown8;
        for (i = 0; i < unknown6; i++) {
            fread(&unknown8, 4, 1, handler);
            printf("\t\033[1;31m%04d", unknown8);
            for (j = 0; j < 4; j++) {
                unknown7 = 0;
                fread(&unknown7, 2, 1, handler);
                printf("\t%05d", unknown7);
            }
            zoneLength -= 12;
            printf("\033[0m\n");
        }

        // IZAX
        // 4 bytes = IZAX
        // 4 bytes = length
        // length - 8 = content
        fread(&izax, 4, 1, handler);
        if (!dta_id_check("IZAX", izax)) {
            printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", izax, "IZAX");
            return EXIT_FAILURE;
        }
        fread(&izaxLength, 4, 1, handler);
        fseek(handler, izaxLength - 8, SEEK_CUR);

        zoneLength -= izaxLength;

        // IZX2
        // 4 bytes = IZX2
        // 4 bytes = length
        // length - 8 = content
        fread(&izx2, 4, 1, handler);
        if (!dta_id_check("IZX2", izx2)) {
            printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", izx2, "IZX2");
            return EXIT_FAILURE;
        }
        fread(&izx2Length, 4, 1, handler);
        fseek(handler, izx2Length - 8, SEEK_CUR);

        zoneLength -= izx2Length;

        // IZX3
        // 4 bytes = IZX3
        // 4 bytes = length (incluyendo estos 8 bytes)
        // length - 8 = content
        fread(&izx3, 4, 1, handler);
        if (!dta_id_check("IZX3", izx3)) {
            printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", izx3, "IZX3");
            return EXIT_FAILURE;
        }
        fread(&izx3Length, 4, 1, handler);
        fseek(handler, izx3Length - 8, SEEK_CUR);

        zoneLength -= izx3Length;
        
        // IZX4 Parece que su longitud es siempre fija y es C, es decir, 12.
        // 4 bytes = IZX4
        // 4 bytes = ?
        // 4 bytes = ?
        fread(&izx4, 4, 1, handler);
        fseek(handler, 8, SEEK_CUR);

        zoneLength -= 12;

        uint8_t* buffer;
        // Si sigue habiendo sitio, entonces leemos los IACT.
        while (zoneLength > 0) {
            
            fread(&iact, 4, 1, handler);
            if (!dta_id_check("IACT", iact)) {            
                printf("\033[1;31mInvalid header %s, expecting %s\033[0m\n", iact, "IACT");
                return EXIT_FAILURE;
            }
            fread(&iactLength, 4, 1, handler);

            fread(&unknown1, 2, 1, handler);
            fread(&unknown2, 2, 1, handler);

            // Guardamos las cosicas en el buffer
            // para empezar a hacer pruebas.
            buffer = (uint8_t*)malloc((iactLength - 4));
            fread(buffer, (iactLength - 4), 1, handler);
 
            //printf("\tOperator?: \033[1;31m%02d %02d\033[0m\n", buffer[0], buffer[1]);

            //fseek(handler, (iactLength - 4), SEEK_CUR);

            //printf("\tAction?: \033[1;31m%05d %05d\033[0m\n", unknown1, unknown2);

            // al tamaño de la zona le restamos el tamaño
            // del IACT más el tamaño de la propia cabecera
            // IACT y la longitud del IACT.
            zoneLength -= (iactLength + 8);

        }

    // ESTOY FLIPANDO
    // ¿Cabe la posibilidad de que los 6 caracteres misteriosos sean simplemente COMENTARIOS?
    
    // 2 bytes (TIPO)
    // 2 bytes ¿SUBTIPO?
    // 2 bytes ¿PARAMETRO?
    // 6 bytes ¿COMENTARIO? ¿NOMBRE?
    // 2 bytes ¿FIN?
    //
    // 00 00 00 00 00 04 00 00 00 00

    // 01 00 02 00
    // 01 00 05 00
    // 01 00 00 00 6E 00 
    //
    // 01 00 05 00
    //
    // 01 00 0B 00 00 00   ne   01 00
    //
    // 01 00 08 00 00 00 Coun   22 00
    // 01 00 0A 00 00 00 Enem   22 00
    //
    // 02 00 02 00
    //
    // 02 00 0A 00 00 00 Enem   22 00
    //
    // 03 00 01 00 00 00 ------ 00 00
    // 03 00 00 00 00 00 ------ 0F 00
    //
    // 04 00 08 00 00 00 ShowTe 
    // 04 00 01 00 1D 03 Bump   14 00
    // 
    // 05 00 04 00 01 00 ShowTe ?¿?¿?¿?¿?¿?
    // 05 00 0B 00 03 00 ------ 
    // 05 00 00 00 00 00 ------ 03 00
    // 05 00 0A 00 00 00 Enem   22 00
    // 05 00 06 00 02 00 move   01 00
    // 05 00 07 00 01 00 move   07 00
    //
    // 06 00 08 00 0A 00 Redraw 00 00
    // 06 00 06 00 10 00 Redraw 00 00
    // 
    // 07 00 01 00 22 04 View   00 00
    // 07 00 00 00 00 00 View   00 00
    //
    // 08 00 08 00 00 00 Coun   00 00
    //
    // 08 00 0A 00 00 00 Hero   04 00
    // 08 00 0B 00 00 00 OpenCh 00 00
    //
    // 09 00 0A 00 00 00 Hero   04 00
    //
    // 09 00 01 00 00 00 WaitFo 00 00
    // 09 00 02 00 00 00 FirstE 00 00
    // 09 00 04 00 02 00 move   01 00
    // 09 00 05 00 01 00 move   07 00
    // 09 00 04 00 09 00 move   08 00
    //
    // 10 00 11 00 00 00 SetHer 
    //
    // 0B 00 03 00 AA 01 Play   22 00
    //
    // 0C 00 03 00 00 00 ThrowR 00 00
    //
    // 0D 00 05 00 XX XX Bump   00 00
    // 0D 00 08 00 00 00 SetCou 00 00
    // 0D 00 00 00 00 00 UseAny 00 00
    // 0D 00 00 00 00 00 Remove 00 00
    // 0D 00 A5 01 00 00 ------ 22 00
    //
    // 0F 00 09 00 63 01 Bump   00 00 01 00
    // 
    // 12 00 0F 00 01 00 SetHer 00 00
    // 12 00 02 00 01 00 SetHer 00 00
    //
    // 14 00 08 00 00 00 MoveHe 00 00
    // 14 00 08 00 00 00 Enter- 00 00
    // 14 00 0C 00 00 00 StartB 00 00
    //
    // 15 00 XX 00 00 00 HotSpo 00 00
    // 
    // 16 00 XX 00 00 00 HotSpo 00 00
    //
    // 17 00 09 00 00 00 EnemyO 00 00
    // 
    // 22 00 01 07 00 00        00 00
    //
    // 24 00 00 00 00 00 SetRan 00 00
    // 24 00 01 00 00 00 SetRan 00 00
    // 24 00 07 00 00 00 SetRan 00 00
    // 24 00 03 00 00 00 SetRan 00 00
    //
    // 04 00 01 00 1D 03 Bump ¿?¿?¿?¿
    // 
    // 0A 00 00 00 00 00 PlaySo
    // 63 00 08 00 00 00 Rand   47 00
    // 65 00 08 00 00 00 Rand   0B 00
    // 6E 00 05 00 00 00 Ente   05 00
    

    // Al parecer dentro de los IACT se pueden guardar
    // scripts de alguna forma que de momento desconozco.
    // Estos parecer ser los comandos más habituales.
    //
    // Tiene pinta de que estos comandos son una palabra
    // de 6 letras y una serie de argumentos.
    //
    // SetHer
    // SetCou
    // WaitFo
    // HotSpo
    // Redraw
    // ShowTe 2b<length> l<string>
    // ShowHe
    // ShowFr
    // DrawTi
    // MoveHe
    // Remove
    // UseAny
    // Invisi
    // ImpOff
    // EnemyO
    // Boushh
    // PlaySo 2b<sound>
    // AddToH
    // AddToC
    // ThrowR
    // MoveTi
    // MoveCo
    // Counte
    // Escape 2b<id>
    // SetGlo
    // RandNu
    // Glob
    // Hero   
    // Bous  
    // Coun  
    // Bump  
    // Ente  
    // Requ  
    // Repl  
    // Read  
    // Rand  
    // SetM  
    // Play  
    // Move  
    // Walk  
    // In the 2b<id>
    //
    // Palabras que se ven a menudo pero que no tengo ni
    // puta idea de por qué
    //
    // ckup (¿?)
    // move (move like mov in asm?)
    // opIt (operator If then?)
    //

        // IACT 
        // 4 bytes = IACT
        // 4 bytes = length
        // length = content

        // NOS SALTAMOS EL CONTENIDO DE LA ZONA. ESTO
        // VA A TENER TELITA.
        //fseek(handler, zoneLength - (24 + (izonWidth * izonHeight * 6)), SEEK_CUR);


    }

    return 1;

}

/**
 * dta_block_read_CHAR()
 *
 * Leemos los personajes.
 */
uint32_t dta_block_read_CHAR(FILE* handler) {

    if (!dta_block_header("CHAR", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    uint32_t items = length / 84;
    uint32_t index = 0;

    uint16_t id = 0;
    char ichar[4];
    char name[16];
    uint32_t unknown1;

    uint16_t image[24];

    printf("Characters: \033[1;31m%d\033[0m\n", items);

    uint8_t path[1024];

    printf("\tCharacter: <ID> <NAME>           <UP>  <DWN> <UP>  <LFT> <DWN> <UP>  <RGT> <ICO> <UP>  <DWN> <UP>  <LFT> <DWN> <UP>  <RGT> <ICO>\n");
    for (index = 0; index < items; index++) {
        
        sprintf(path, "CHARS/%04d.json", index);

        FILE* charHandler = fopen(path, "w");
        if (charHandler == NULL) {
            printf("\033[1;31mCannot create file\033[0m\n");
            return EXIT_FAILURE;
        }

        fprintf(charHandler,"[\n");

        fread(&id, 2, 1, handler);
        fread(&ichar, 4, 1, handler);
        if (!dta_id_check("ICHA", ichar)) {
            printf("\033[1;31mInvalid id ICHA\033[0m\n");
            return EXIT_FAILURE;
        }

        fread(&unknown1, 4, 1, handler); 
        fread(&name, 16, 1, handler);

        fseek(handler, 4, SEEK_CUR); // 0x00000001
        fseek(handler, 2, SEEK_CUR); // 0xFFFF
        fseek(handler, 4, SEEK_CUR); // 0x00000000

        fread(&image[0], 2, 1, handler);  // +2 2       UP
        fread(&image[1], 2, 1, handler);  // +2 4       DN
        fread(&image[2], 2, 1, handler);  // +2 6       UP
        fread(&image[3], 2, 1, handler);  // +2 8       LF
        fread(&image[4], 2, 1, handler);  // +2 10      DN
        fread(&image[5], 2, 1, handler);  // +2 12      UP
        fread(&image[6], 2, 1, handler);  // +2 14      RT
        fread(&image[7], 2, 1, handler);  // +2 16      DN
        fread(&image[8], 2, 1, handler);  // +2 18      UP
        fread(&image[9], 2, 1, handler);  // +2 20      DN
        fread(&image[10], 2, 1, handler); // +2 22      UP
        fread(&image[11], 2, 1, handler); // +2 24      LF
        fread(&image[12], 2, 1, handler); // +2 26      DN
        fread(&image[13], 2, 1, handler); // +2 28      UP
        fread(&image[14], 2, 1, handler); // +2 30      RT
        fread(&image[15], 2, 1, handler); // +2 32      DN
        fread(&image[16], 2, 1, handler); // +2 34      UP
        fread(&image[17], 2, 1, handler); // +2 36      DN
        fread(&image[18], 2, 1, handler); // +2 38      UP
        fread(&image[19], 2, 1, handler); // +2 40      RT
        fread(&image[20], 2, 1, handler); // +2 42      DN
        fread(&image[21], 2, 1, handler); // +2 44      DN
        fread(&image[22], 2, 1, handler); // +2 46      DN
        fread(&image[23], 2, 1, handler); // +2 48      DN

        fprintf(charHandler, "\t{\n");
        fprintf(charHandler, "\t\t\"name\": \"%s\",\n", name);
        fprintf(charHandler, "\t\t\"images\": [\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d,\n\t\t\t%d\n\t\t]\n", image[0], image[1], image[2], image[3], image[4], image[5], image[6], image[7], image[8], image[9], image[10], image[11], image[12], image[13], image[14], image[15], image[16], image[17], image[18], image[19], image[20], image[21], image[22], image[23]);

        if (index == items - 1) {
            fprintf(charHandler, "\t}\n");
        } else {
            fprintf(charHandler, "\t},\n");    
        }

        printf("\tCharacter: \033[1;31m%04d %-16s %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d %05d\033[0m\n", 
            id, 
            name, 
            image[0], 
            image[1], 
            image[2], 
            image[3],
            image[4], 
            image[5], 
            image[6], 
            image[7],
            image[8], 
            image[9], 
            image[10], 
            image[11],
            image[12], 
            image[13], 
            image[14], 
            image[15],
            image[16], 
            image[17], 
            image[18], 
            image[19],
            image[20], 
            image[21], 
            image[22], 
            image[23]
        );

        //fseek(handler, 40, SEEK_CUR);

        fprintf(charHandler,"]\n");

        fclose(charHandler);
    
    }

    fseek(handler, 2, SEEK_CUR);

    return 1;

}

/**
 * Leemos el final del archivo.
 */
uint32_t dta_block_read_ENDF(FILE* handler) {

    if (!dta_block_header("ENDF", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    if (length == 0)
        return 1;

    return EXIT_FAILURE;

}

/**
 * Leemos la ¿tabla de generación?
 */
uint32_t dta_block_read_TGEN(FILE* handler) {

    if (!dta_block_header("TGEN", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    fseek(handler, length, SEEK_CUR);

    return 1;

}

/**
 * Leemos la tabla de nombres.
 */
uint32_t dta_block_read_TNAM(FILE* handler) {

    if (!dta_block_header("TNAM", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    uint32_t items = length / 26;
    uint32_t index = 0;
    uint16_t id;
    
    char name[24];

    char path[1024];

    sprintf(path, "name.json");
    FILE* nameHandler = fopen(path, "w");
    if (nameHandler == NULL) {
        printf("\033[1;31mCannot create file\033[0m");
        return EXIT_FAILURE;
    }

    fprintf(nameHandler, "{\n");

    for (index = 0; index < items; index++) {
        fread(&id, 2, 1, handler);
        fread(&name, 24, 1, handler);
        printf("\tName: \033[1;31m%05d\t%s\033[0m\n", id, name);
        
        fprintf(nameHandler, "\t\"%d\": \"%s\"", id, name);

        if (index == items - 1) {
            fprintf(nameHandler, "\n");
        } else {
            fprintf(nameHandler, ",\n");
        }

    }

    fprintf(nameHandler, "}\n");

    fseek(handler, 2, SEEK_CUR);
    //uint16_t rubbish;
    //fread(&rubbish,2,1,handler);

    fclose(nameHandler);

    return 1;

}

/**
 * Leemos el CHWP
 */
uint32_t dta_block_read_CHWP(FILE* handler) {
    
    if (!dta_block_header("CHWP", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    uint32_t items = length / 6;
    uint32_t index = 0;
    uint16_t unknown1 = 0;
    uint16_t weapon = 0;

    uint16_t id = 0;

    printf("\tWeapon: <CH> <WPN> <UNK>\n");
    for (index = 0; index < items; index++) {
        fread(&id, 2, 1, handler);
        fread(&weapon, 2, 1, handler);
        fread(&unknown1, 2, 1, handler);
        printf("\tWeapon: \033[1;31m%04d %05d %05d\033[0m\n", id, weapon, unknown1);
    }

    fseek(handler, 2, SEEK_CUR);

    return 1;

}

/**
 * Leemos los CAUX
 */
uint32_t dta_block_read_CAUX(FILE* handler) {

    if (!dta_block_header("CAUX", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    fseek(handler, length, SEEK_CUR);

    return 1;

}

/**
 * Leemos los puzzles.
 */
uint32_t dta_block_read_PUZ2(FILE* handler) {

    if (!dta_block_header("PUZ2", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    
    uint8_t ipuz[4];

    uint16_t id = 0;

    uint32_t puzzleLength = 0;
    uint32_t puzzleTexts = 0;

    uint16_t textLength = 0;
    uint16_t textCount = 0;

    uint32_t unknown1 = 0;
    uint32_t unknown2 = 0;
    uint32_t unknown3 = 0;
    uint16_t unknown4 = 0;
    uint16_t unknown5 = 0;
    uint32_t unknown6 = 0;

    uint32_t index = 0;
    uint32_t offset = 0;

    uint8_t text[4096];
    uint8_t path[1024];
   
    printf("\tPuzzle: <ID>\t<SECT>\t<¿?>\t<¿?>\t<¿FLAGS?>\t<¿FLAGS?>\n");
    while (id != 0xFFFF) {
        
        fread(&id, 2, 1, handler);
        if (id == 0xFFFF) {
            break;
        }
        
        memset(&ipuz, 0, 4);

        fread(&ipuz, 4, 1, handler);
        if (!dta_id_check("IPUZ", ipuz)) {
            printf("\033[1;31mExpected IPUZ, read \"%s\"\033[0m\n", ipuz);
            return EXIT_FAILURE;
        }

        fread(&puzzleLength, 4, 1, handler);

        // UNKNOWN 1 (FLAGS)?
        // 
        // Su valor normalmente oscila entre 0, 1, 2 y 3. 0x00 0x01 0x10 0x11
        //
        fread(&unknown1, 4, 1, handler);

        // UNKNOWN 2 (TIPO DE MISIÓN?)
        // 
        // Su valor oscila entre 0, 1, 2 y 4 ¿Banderas? 0x00 0x01 0x02 0x04
        //
        fread(&unknown2, 4, 1, handler);

        // UNKNOWN 3
        // 
        // ¿?¿?¿?¿? este normalmente es 0, 4 o 0xFFFFFFFF
        //
        fread(&unknown3, 4, 1, handler);

        // UNKNOWN 4
        // 
        // Este es curioso pero suele ser o bien 0x0000, 0x0080, 0x011F o bien 0x013F.
        //
        // 0x0000   0000 0000 0000 0000
        // 0x0080   0000 0000 1000 0000
        // 0x011F   0000 0001 0001 1111
        // 0x013F   0000 0001 0011 1111
        // 0x0137   0000 0001 0011 0111
        //
        fread(&unknown4, 2, 1, handler);

        offset = 4 + 4 + 4 + 2;

        printf("\tPuzzle: \033[1;31m%04d\t%06d\t%04d\t%X\t%08X\t%03X\033[0m\n", id, puzzleLength, unknown1, unknown2, unknown3, unknown4);

        sprintf(path,"PUZZLES/%04d.json", id);

        FILE* puzzleHandler = fopen(path, "w");
        if (puzzleHandler == NULL) {
            printf("\033[1;31mCannot create file %s\033[0m\n", path);
            return EXIT_FAILURE;
        }

        fprintf(puzzleHandler, "{\n");

        fprintf(puzzleHandler, "\t\"id\": %d,\n", id);
        fprintf(puzzleHandler, "\t\"type\": %d,\n", unknown1);
        fprintf(puzzleHandler, "\t\"flags\": %d,\n", unknown2);

        fprintf(puzzleHandler, "\t\"tiles\": [\n");
        textLength = 0xFFFF;
        while (textLength != 0) {
            
            fread(&textLength, 2, 1, handler);
            offset += 2;

            memset(&text, 0, 4096);

            fread(&text, textLength, 1, handler);
            offset += textLength;

            if (textLength != 0) {
                fprintf(puzzleHandler, "\t\t\"%s\",\n", text);
            } else {
                fprintf(puzzleHandler, "\t\t\"%s\"\n", text);
            }

            printf("%s\n", text);

        }
        fprintf(puzzleHandler, "\t]\n");

        fprintf(puzzleHandler, "}\n");

        fclose(puzzleHandler);

        //fseek(handler, 4, SEEK_CUR);

        /*if (unknown1 == 1) {
            fseek(handler, 4, SEEK_CUR);
            offset += 4;
        } else if (unknown1 == 3) {
            fseek(handler, 2, SEEK_CUR);
            offset += 2;
        }

        textCount = 0;
        textLength = 0xFFFF;
        while (textLength != 0) {
            
            textLength = 0;

            fread(&textLength, 2, 1, handler);
            offset += 2;

            printf("%d\n", textLength);
            if (textLength > 0) {

                memset(&text, 0, 4096);

                fread(&text, textLength, 1, handler);
                offset += textLength;

                printf("\t\t%s\n", text);

            }

            if (unknown1 == 2 && unknown3 == 0xFFFFFFFF && textCount == 0) {
                fseek(handler, 2, SEEK_CUR);
                offset += 2;
            }

            textCount++;

        }*/

        fseek(handler, puzzleLength - offset, SEEK_CUR);

    }

    return 1;

}

/**
 * dta_block_read_TILE()
 *
 * Leemos los tiles.
 */
uint32_t dta_block_read_TILE(FILE* handler, uint8_t* paletteFilename) {

    if (!dta_block_header("TILE", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    uint32_t offset = 0;

    uint32_t index = 0;

    uint8_t tile[1024];    
    uint8_t path[1024];

    FILE* palette;
    FILE* out;

    uint32_t flags = 0;

    uint8_t notWrittingTile = 0;
    uint8_t notWrittingFlags = 0;
    uint8_t notWrittingPng = 0;

    uint8_t colors[768];
    palette = fopen(paletteFilename, "r");
    if (palette != NULL) {
        fread(&colors, 768, 1, palette);
    }

    while (offset < length) {

        fread(&flags, 4, 1, handler);

        // aumentamos el offset por el tamaño de las
        // flags
        offset += sizeof(uint32_t);

        // leemos el contenido del tile.
        fread(&tile, DTA_TILE_SIZE, 1, handler);
        
        // escribimos el tile.
        sprintf(path, "TILES/%04d.tile", index);

        if (access(path, F_OK) != 0) {
            
            out = fopen(path, "w");
            if (out != NULL) {
                fwrite(tile, DTA_TILE_SIZE, 1, out);
                fclose(out);
            }

        } else {

            notWrittingTile = 1;

        }

        sprintf(path, "TILES/%04d.flags", index);

        if (access(path, F_OK) != 0) {
            
            out = fopen(path, "w");
            if (out != NULL) {
                fwrite(&flags, 4, 1, out);
                fclose(out);
            }

        } else {

            notWrittingFlags = 1;

        }

        if (palette != NULL) {

            sprintf(path, "TILES/%04d.png", index);

            if (access(path, F_OK) != 0) {

                write_png(&path, &tile, &colors);

            } else {

                notWrittingPng = 1;

            }

        }

        printf("\tTile: \033[1;31m%04d", index);
        if (notWrittingTile) {
            printf("\t!TILE");
        }

        if (notWrittingFlags) {
            printf("\t!FLAGS");
        }

        if (notWrittingPng) {
            printf("\t!PNG");
        }

        printf("\033[0m\n");

        // aumentamos el tamaño del offset por el tamaño
        // del buffer del tile.
        offset += 1024;

        index++;
        
    }
    
    if (palette != NULL) {
        fclose(palette);
    }

    return 1;

}

/**
 * Leemos el listado de sonidos.
 */
uint32_t dta_block_read_SNDS(FILE* handler) {

    if (!dta_block_header("SNDS", handler)) {
        return EXIT_FAILURE;
    
    }

    uint32_t length = dta_block_length(handler);

    uint8_t reserved = 0;
    fread(&reserved, sizeof(uint8_t), 1, handler);
    fread(&reserved, sizeof(uint8_t), 1, handler);
    
    uint16_t stringLength;
    uint8_t* stringContent = (uint8_t*)malloc(1024);
    uint16_t index = 0;

    uint32_t offset = 2;
    while (offset < length) {
        
        fread(&stringLength, sizeof(uint16_t), 1, handler);
        offset += sizeof(uint16_t);

        fread(stringContent, stringLength, 1, handler);
        offset += stringLength;

        printf("\tSound: \033[1;31mi%05d\t%s\033[0m\n", index, stringContent);
        
        memset(stringContent, 0, 1024);
        index++;

    }

    free((void*)stringContent);
    return 1;

}

/**
 * Leemos el bloque STUP que es básicamente
 * la imagen de introducción.
 */
uint32_t dta_block_read_STUP(FILE* handler) {

    if (!dta_block_header("STUP", handler)) {
        return EXIT_FAILURE;
    }

    uint32_t length = dta_block_length(handler);
    if (length != 288 * 288) {
        return EXIT_FAILURE;
    }

    // cargamos la imagen en memoria
    uint8_t* image = (uint8_t*)malloc(length);
    fread(image, length, 1, handler);

    FILE* out = fopen("main.scr", "w");
    if (out != NULL) {
        fwrite(image, length, 1, out);
        fclose(out);
    }

    // y la eliminamos.
    free((void*)image);
    return 1;

}

/**
 * Leemos la versión del archivo.
 */
uint32_t dta_vers(FILE* handler) {


    if (!dta_block_header("VERS", handler))
        return EXIT_FAILURE;

    dta_version_t version;
    fread(&version, 4, 1, handler);
    printf("Version is \033[1;31m%d.%d.%d\033[0m\n", version.major, version.minor, version.revision);

    return 1;

}

/**
 * write_png
 *
 *  Este método será el encargado de guardar la imagen.
 *
 */
int write_png(uint8_t* filename, uint8_t* image, uint8_t* palette) {

    int width = 32;
    int height = 32;
    FILE* fp;
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep row;

    fp = fopen(filename, "wb");
    if (fp == NULL) {
        return EXIT_FAILURE;
    }

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        fclose(fp);
        return EXIT_FAILURE;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        fclose(fp);
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return EXIT_FAILURE;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        fclose(fp);
        png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return EXIT_FAILURE;
    }

    png_init_io(png_ptr, fp);

    png_set_IHDR(
            png_ptr, 
            info_ptr, 
            width,     // width
            height,     // height
            8, 
            PNG_COLOR_TYPE_RGB, 
            PNG_INTERLACE_NONE, 
            PNG_COMPRESSION_TYPE_BASE, 
            PNG_FILTER_TYPE_BASE
        );

    png_write_info(png_ptr, info_ptr);

    row = (png_bytep) malloc(3 * width * sizeof(png_byte));

    int x,y;
    for (y = 0; y < height; y++) {

        for (x = 0; x < width; x++) {
            setRGB(&(row[x * 3]), &(palette[image[y * width + x] * 3]));
        }
        
        png_write_row(png_ptr, row);

    }

    png_write_end(png_ptr, NULL);

    fclose(fp);
    
    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

    return 1;

}

/**
 * main.c
 *
 *  Este archivo se encarga de extraer el contenido del archivo DTA
 * tanto del Yoda Stories como de Indiana Jones.
 *
 */
int main(int argc, uint8_t** argv) {
    if (argc <= 2) {
        printf("How to use this program\n\tdta <filename> <palette>\n\n");
        return EXIT_FAILURE;
    }

    FILE* handler;
    
    uint8_t* file = argv[1];
    uint8_t* palette = argv[2];

    // si no podemos abrir el archivo pasado como parámetro mostramos
    // un error en pantalla.
    if ((handler = fopen(file,"r")) == NULL) {
        printf("Can't read: %s\n", file);
        return EXIT_FAILURE;
    }

    // Mostramos que hemos podido abrir el archivo.
    printf("Opening: \033[1;31m%s\033[0m\n", file);

    if (!dta_vers(handler)) {
        return EXIT_FAILURE;
    }

    // STUP section.
    if (!dta_block_read_STUP(handler))
        return EXIT_FAILURE;

    // SNDS section.
    if (!dta_block_read_SNDS(handler)) {
        return EXIT_FAILURE;
    }

    // TILE section.
    if (!dta_block_read_TILE(handler, palette)) {
        return EXIT_FAILURE;
    }

    // ZONE section.
    if (!dta_block_read_ZONE(handler)) {
        return EXIT_FAILURE;
    }
    
    // PUZ2 section.
    if (!dta_block_read_PUZ2(handler)) {
        return EXIT_FAILURE;
    }
    
    // CHAR section.
    if (!dta_block_read_CHAR(handler)) {
        return EXIT_FAILURE;
    }

    // CHWP section.
    if (!dta_block_read_CHWP(handler)) {
        return EXIT_FAILURE;
    }

    // CAUX section.
    if (!dta_block_read_CAUX(handler)) {
        return EXIT_FAILURE;
    }

    // TNAM section.
    if (!dta_block_read_TNAM(handler)) {
        return EXIT_FAILURE;
    }
    
    // TGEN section.
    if (!dta_block_read_TGEN(handler)) {
        return EXIT_FAILURE;
    }
    
    // ENDF section.
    if (!dta_block_read_ENDF(handler)) {
        return EXIT_FAILURE;
    }

    // PUZ2 section.
    //dta_block_read_puzzles(handler);

    // BLOQUES, BLOQUES, BLOQUES

    // PUZ2
    // IPUZ

    // IZON
    // IACT
    // IZAX
    // IZX2
    // IZX1
    // IZX3
    // IZX4

    // CHAR
    // ICHA
    //
    // TNAM
    // TGEN
    // ENDF

    // cerramos el archivo.
    fclose(handler);
    return EXIT_SUCCESS;

}

const fileReaderFiles = new Map();

let palette;

function readID(dataView, offset, expectedID) {
  const id = dataView.getUint32(offset);
  const strID = String.fromCharCode((id >> 24) & 0xFF)
              + String.fromCharCode((id >> 16) & 0xFF)
              + String.fromCharCode((id >> 8) & 0xFF)
              + String.fromCharCode(id & 0xFF);
  if (strID !== expectedID) {
    throw new Error(`Invalid id ${strID}, expected ${expectedID}`);
  }
  return strID;
}

function readVersion(dataView, offset, expectedVersion) {
  const major = dataView.getUint16(offset);
  const minor = dataView.getUint8(offset + 2);
  const revision = dataView.getUint8(offset + 3);
  const [expectedMajor,expectedMinor,expectedRevision] = expectedVersion.split(".").map(parseInt);
  if (major !== expectedMajor
   && minor !== expectedMinor
   && revision !== expectedRevision) {
    throw new Error(`Invalid version ${major}.${minor}.${revision}, expected ${expectedMajor}.${expectedMinor}.${expectedRevision}`);
  }
  return {
    major, minor, revision
  };
}

function readLength(dataView, offset) {
  return dataView.getUint32(offset, true);
}

function parseData(arrayBuffer) {
  const dataView = new DataView(arrayBuffer);

  /**
   * This part reads the version.
   */
  readID(dataView, 0, "VERS");
  readVersion(dataView, 4, "2.0.0");
  
  /**
   * This part reads the setup screen.
   */
  readID(dataView, 8, "STUP");
  setupLength = readLength(dataView, 12);
  const canvas = document.createElement("canvas");
  canvas.width = 288;
  canvas.height = 288;
  const context = canvas.getContext("2d");
  const imageData = new ImageData(canvas.width,canvas.height);
  for (let y = 0; y < canvas.height; y++) {
    for (let x = 0; x < canvas.width; x++) {
      const colorIndex = dataView.getUint8(16 + (y * canvas.width) + x);
      imageData.data[((y * canvas.width) + x) * 4] = palette[(colorIndex * 3) + 2];
      imageData.data[(((y * canvas.width) + x) * 4) + 1] = palette[(colorIndex * 3) + 1];
      imageData.data[(((y * canvas.width) + x) * 4) + 2] = palette[(colorIndex * 3) + 0];
      imageData.data[(((y * canvas.width) + x) * 4) + 3] = 255;
    }
  }
  context.putImageData(imageData, 0, 0);
  document.body.appendChild(canvas);

  /**
   * This part reads sounds.
   */
  readID(dataView, 16 + setupLength, "SNDS");
  soundsLength = readLength(dataView, 20 + setupLength);
  let offset = 2;
  while (offset < soundsLength) {
    const stringLength = dataView.getUint16(24 + setupLength + offset, true);
    let string = "";
    for (let index = 0; index < stringLength; index++) {
      string += String.fromCharCode(dataView.getUint8(24 + setupLength + offset + 2 + index));
    }
    console.log(string);
    offset += stringLength + 2;
  }
  

  /*
  readID(dataView, 24 + soundsLength, "TILE");
  tileLength = readLength(dataView, 24 + soundsLength);

  readID(dataView, 24 + tileLength, "ZONE");
  zoneLength = readLength(dataView, 28 + tileLength);

  readID(dataView, 28 + zoneLength, "PUZ2");
  puzzlesLength = readLength(dataView, 32 + zoneLength);

  readID(dataView, 32 + puzzlesLength, "CHAR");
  charactersLength = readLength(dataView, 36 + puzzlesLength);

  readID(dataView, 36 + charactersLength, "CHWP");
  characterWeaponLength = readLength(dataView, 40 + charactersLength);

  readID(dataView, 40 + characterWeaponLength, "CAUX");
  characterAuxLength = readLength(dataView, 44 + characterWeaponLength);

  readID(dataView, 44 + characterAuxLength, "TNAM");
  tileNameLength = readLength(dataView, 48 + characterAuxLength);

  readID(dataView, 48 + tileNameLength, "TGEN");
  tileGenerationLength = readLength(dataView, 52 + tileNameLength);

  readID(dataView, 52 + tileGenerationLength, "ENDF");
  */

  /*

    // SNDS section.
    if (!dta_block_read_SNDS(handler)) {
        return EXIT_FAILURE;
    }

    // TILE section.
    if (!dta_block_read_TILE(handler, palette)) {
        return EXIT_FAILURE;
    }

    // ZONE section.
    if (!dta_block_read_ZONE(handler)) {
        return EXIT_FAILURE;
    }
    
    // PUZ2 section.
    if (!dta_block_read_PUZ2(handler)) {
        return EXIT_FAILURE;
    }
    
    // CHAR section.
    if (!dta_block_read_CHAR(handler)) {
        return EXIT_FAILURE;
    }

    // CHWP section.
    if (!dta_block_read_CHWP(handler)) {
        return EXIT_FAILURE;
    }

    // CAUX section.
    if (!dta_block_read_CAUX(handler)) {
        return EXIT_FAILURE;
    }

    // TNAM section.
    if (!dta_block_read_TNAM(handler)) {
        return EXIT_FAILURE;
    }
    
    // TGEN section.
    if (!dta_block_read_TGEN(handler)) {
        return EXIT_FAILURE;
    }
    
    // ENDF section.
    if (!dta_block_read_ENDF(handler)) {
        return EXIT_FAILURE;
    }

    // PUZ2 section.*/
}

function parsePalette(arrayBuffer) {
  palette = new Uint8Array(arrayBuffer);
}

function handleProgress(e) {
  console.log(e.type);
  const fr = e.target;
  if (e.type === "abort" 
   || e.type === "error" 
   || e.type === "load") {
    fr.removeEventListener("abort", handleProgress);
    fr.removeEventListener("error", handleProgress);
    fr.removeEventListener("loadstart", handleProgress);
    fr.removeEventListener("loadend", handleProgress);
    fr.removeEventListener("load", handleProgress);
    fr.removeEventListener("progress", handleProgress);
  }

  if (e.type === "error") {
    console.error(fr.error);
  } else if (e.type === "load") {
    if (fileReaderFiles.has(fr)) {
      const file = fileReaderFiles.get(fr);
      fileReaderFiles.delete(fr);
      if (fr.result) {
        if (file.name === "YODESK.DTA") {
          console.log("Parse data");
          parseData(fr.result);
        } else if (file.name === "YODESK.ACT") {
          console.log("Parse palette");
          parsePalette(fr.result);
        }
      }
    }
  }
}

function handleDrop(e) {
  if (e.type === "dragover") {
    e.preventDefault();
  } else if (e.type === "drop") {
    e.preventDefault();
    console.log(e.dataTransfer.items);
    for (let item of e.dataTransfer.items) {
      if (item.kind === "file") {
        const file = item.getAsFile();
        if (file.name === "YODESK.DTA"
         || file.name === "YODESK.ACT") {
          const fr = new FileReader();
          fr.addEventListener("abort", handleProgress);
          fr.addEventListener("error", handleProgress);
          fr.addEventListener("loadstart", handleProgress);
          fr.addEventListener("loadend", handleProgress);
          fr.addEventListener("load", handleProgress);
          fr.addEventListener("progress", handleProgress);
          fr.readAsArrayBuffer(file);
          fileReaderFiles.set(fr, file);
        }
      }
    }
  }
}

document.addEventListener("dragover", handleDrop);
document.addEventListener("drop", handleDrop);